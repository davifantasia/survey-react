import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  answerQuestion,
  setCurrentQuestion
} from '../../action-creators/questions';
import { Input, Dropdown, Form, Radio } from 'semantic-ui-react';
import './Survey.css';

type Props = {
  match: Object,
  setCurrentQuestion: Function,
  questions: Array<{
    id: number,
    question: string,
    type: string,
    options?: Array<string>,
    answer?: string
  }>,
  answerQuestion: Function
};

class Survey extends Component<Props> {
  componentDidUpdate() {
    this.updateCurrentQuestion();
  }
  render() {
    let { id } = this.props.match.params;
    let question = this.props.questions.find(question => question.id === +id);

    if (question) {
      return this.renderQuestion(question);
    }

    return (
      <div className="Survey">
        <p>Problem finding details for question {id}</p>
      </div>
    );
  }

  renderQuestion = question => {
    return (
      <div className="Survey">
        <p className="Survey__question">Question: {question.question}</p>
        {this.renderQuestionType(question)}
      </div>
    );
  };

  renderQuestionType = question => {
    switch (question.type) {
    case 'text':
      return this.renderText();
    case 'dropdown':
      return this.renderDropdown(question.options);
    case 'radio':
      return this.renderRadioGroup(question.options);
    default:
      return this.renderDefault(question.id);
    }
  };

  renderText = () => {
    return (
      <Input
        onChange={this.handleOnChange}
        type="number"
        value={this.currentQuestionAnswer()}
      />
    );
  };

  renderDropdown = (options: Array<string>) => {
    const optionsObject = options.map(option => ({
      text: option,
      value: option
    }));
    return (
      <Dropdown
        placeholder="Select Option"
        fluid
        selection
        options={optionsObject}
        onChange={this.handleOnChange}
        value={this.currentQuestionAnswer()}
      />
    );
  };

  renderRadioGroup = (options: Array<string>) => {
    return (
      <Form>
        {Object.keys(options).map(key => this.renderRadio(key, options))}
      </Form>
    );
  };

  renderRadio = (key, options) => {
    return (
      <Form.Field key={key}>
        <Radio
          label={options[key]}
          name="radioGroup"
          value={options[key]}
          checked={this.currentQuestionAnswer() === options[key]}
          onChange={this.handleOnChange}
        />
      </Form.Field>
    );
  };

  handleOnChange = (event, { value }) => {
    let { id } = this.props.match.params;
    this.props.answerQuestion(+id, value);
  };

  currentQuestionAnswer = (): string => {
    let { id } = this.props.match.params;
    let { questions } = this.props;

    let question = questions.find(question => {
      return question.id === +id;
    });

    if (question && question.answer) {
      return question.answer;
    }

    return '';
  };

  renderDefault = (id: number) => {
    return (
      <div>
        <p>Problem finding question type for question {id}</p>
      </div>
    );
  };

  componentDidMount() {
    this.updateCurrentQuestion();
  }

  updateCurrentQuestion = () => {
    let { id } = this.props.match.params;
    this.props.setCurrentQuestion(+id);
  };
}

const mapStateToProps = state => ({
  questions: state.questions
});

export default connect(
  mapStateToProps,
  { setCurrentQuestion, answerQuestion }
)(Survey);

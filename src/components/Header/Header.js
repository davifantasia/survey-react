import React from 'react';
import PropTypes from 'prop-types';
import { TITLE } from '../../utils/strings';
import './Header.css';

const Header = () => (
  <header className="Header">
    <h1 className="Header__title">{TITLE}</h1>
  </header>
);

Header.propTypes = {
  title: PropTypes.string
};

export default Header;

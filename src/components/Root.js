import React from 'react';
import PropTypes from 'prop-types';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Route /*, Link*/ } from 'react-router-dom';

import Header from './Header/Header';
import Home from './Home/Home';
import Survey from './Survey/Survey';
import Summary from './Summary/Summary';
import Footer from './Footer/Footer';

type Props = {
  store: Object
};

const Root = (props: Props) => (
  <Provider store={props.store}>
    <Router>
      <div style={{ width: '100%', height: '100%' }}>
        <Header />
        <Route path="/" exact component={Home} />
        <Route path="/survey/:id" exact component={Survey} />
        <Route path="/summary" component={Summary} />
        <Footer />
      </div>
    </Router>
  </Provider>
);

Root.propTypes = {
  store: PropTypes.object
};

export default Root;

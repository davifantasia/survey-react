import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect, withRouter } from 'react-router-dom';
import { Button } from 'semantic-ui-react';
import './Home.css';

type Props = {
  currentQuestion: number,
  firstQuestionId: number,
  history: Object
};

export class Home extends Component<Props> {
  render() {
    if (this.props.currentQuestion) {
      return <Redirect to={`/survey/${this.props.currentQuestion}`} />;
    }
    return (
      <div className="Home">
        <Button color="green" onClick={this.beginSurvey}>
          Begin
        </Button>
      </div>
    );
  }

  beginSurvey = () => {
    this.props.history.push(`/survey/${this.props.firstQuestionId}`);
  };
}

const mapStateToProps = state => ({
  currentQuestion: state.currentQuestion,
  firstQuestionId: state.questions[0].id
});

export default connect(mapStateToProps)(withRouter(Home));

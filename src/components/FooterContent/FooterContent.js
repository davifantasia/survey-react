import React from 'react';
import { connect } from 'react-redux';
import { BACK, NEXT, FINISH } from '../../utils/strings';
import { Button } from 'semantic-ui-react';
import './FooterContent.css';

type Props = {
  currentQuestion: number,
  currentQuestionAnswer: string,
  isFirstQuestion: boolean,
  isLastQuestion: boolean,
  onBackClick: Function,
  onNextClick: Function
};

const FooterContent = (props: Props) => (
  <div className="FooterContent">
    <Button
      className="FooterContent__button"
      color="green"
      onClick={props.onBackClick}
      disabled={props.isFirstQuestion}
    >
      {BACK}
    </Button>
    <Button
      className="FooterContent__button"
      color="green"
      onClick={props.onNextClick}
      disabled={!props.currentQuestionAnswer}
    >
      {props.isLastQuestion ? FINISH : NEXT}
    </Button>
  </div>
);

const mapStateToProps = state => ({
  currentQuestion: state.currentQuestion
});

export default connect(mapStateToProps)(FooterContent);

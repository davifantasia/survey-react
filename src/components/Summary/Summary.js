import React, { Component } from 'react';
import { connect } from 'react-redux';
import { setCurrentQuestion } from '../../action-creators/questions';
import { SUMMARY } from '../../utils/strings';
import './Summary.css';

type Props = {
  setCurrentQuestion: Function,
  questions: Array<{
    id: number,
    question: string,
    answer: string
  }>
};

export class Summary extends Component<Props> {
  componentDidUpdate() {
    this.props.setCurrentQuestion(0);
  }

  render() {
    return (
      <div className="Summary">
        <h2 className="Summary__title">{SUMMARY}</h2>

        {Object.keys(this.props.questions).map(key => this.renderQuestion(key))}
      </div>
    );
  }

  renderQuestion = key => {
    return (
      <div key={key} className="Summary__question-container">
        <p className="Summary__question">
          Question: {this.props.questions[key].question}
        </p>
        <p className="Summary__answer">
          Your Response: {this.props.questions[key].answer}
        </p>
      </div>
    );
  };

  componentDidMount() {
    this.props.setCurrentQuestion(0);
  }
}

const mapStateToProps = state => ({
  questions: state.questions
});

export default connect(
  mapStateToProps,
  { setCurrentQuestion }
)(Summary);

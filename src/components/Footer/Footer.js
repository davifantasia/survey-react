import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import FooterContent from '../FooterContent/FooterContent';
import './Footer.css';

type Props = {
  currentQuestion: number,
  questions: Array<{
    id: number,
    question: string,
    type: string,
    options?: Array<string>,
    answer?: string
  }>,
  history: Object
};

class Footer extends Component<Props> {
  render() {
    return (
      <footer className={`Footer ${!this.props.currentQuestion ? 'hide' : ''}`}>
        <FooterContent
          currentQuestionAnswer={this.currentQuestionAnswer()}
          isFirstQuestion={this.isFirstQuestion()}
          isLastQuestion={this.isLastQuestion()}
          onBackClick={this.handleBack}
          onNextClick={this.handleNext}
        />
      </footer>
    );
  }

  currentQuestionAnswer = (): string => {
    let { questions, currentQuestion } = this.props;

    let question = questions.find(question => {
      return question.id === currentQuestion;
    });

    if (question && question.answer) {
      return question.answer;
    }

    return '';
  };

  isFirstQuestion = (): boolean => {
    return this.currentQuestionIndex() === 0;
  };

  isLastQuestion = (): boolean => {
    return this.currentQuestionIndex() === this.props.questions.length - 1;
  };

  handleBack = () => {
    let previousQuestion = this.previousQuestion();
    if (previousQuestion) {
      this.props.history.push(`/survey/${previousQuestion}`);
    }
  };

  handleNext = () => {
    let nextQuestion = this.nextQuestion();

    if (nextQuestion) {
      this.props.history.push(`/survey/${nextQuestion}`);
    } else {
      this.props.history.push('/summary');
    }
  };

  previousQuestion = (): number => {
    let { questions } = this.props;

    let currentQuestionIndex = this.currentQuestionIndex();

    if (currentQuestionIndex > 0) {
      return questions[currentQuestionIndex - 1].id;
    }

    return 0;
  };

  nextQuestion = (): number => {
    let { questions } = this.props;

    let currentQuestionIndex = this.currentQuestionIndex();

    if (currentQuestionIndex < questions.length - 1) {
      return questions[currentQuestionIndex + 1].id;
    }

    return 0;
  };

  currentQuestionIndex = (): number => {
    let { questions, currentQuestion } = this.props;

    return questions.findIndex(question => {
      return question.id === currentQuestion;
    });
  };
}

const mapStateToProps = state => ({
  currentQuestion: state.currentQuestion,
  questions: state.questions
});

export default connect(mapStateToProps)(withRouter(Footer));

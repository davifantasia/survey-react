export const answerQuestion = (id: number, answer: string) => (
  dispatch: Function
) => {
  dispatch({
    type: 'ANSWER_QUESTION',
    id,
    answer
  });
};

export const setCurrentQuestion = (id: number) => (dispatch: Function) => {
  dispatch({
    type: 'SET_CURRENT_QUESTION',
    id
  });
};

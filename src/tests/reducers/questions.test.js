import questions from '../../reducers/questions';

describe('Questions Reducer', () => {
  const stateBefore = [
    {
      id: 0,
      question: 'Where did you live in the summer of 2018?',
      type: 'text'
    },

    {
      id: 1,
      question: 'Choose 1 thing to add to your bucket list.',
      type: 'dropdown',
      options: ['Bungee Jumping', 'Sky Diving']
    }
  ];

  Object.freeze(stateBefore);

  it('has intended initial state', () => {
    expect(questions(stateBefore, {})).toEqual(stateBefore);
  });

  it('answer question', () => {
    const stateAfter = [
      {
        id: 0,
        question: 'Where did you live in the summer of 2018?',
        type: 'text',
        answer: '16'
      },

      {
        id: 1,
        question: 'Choose 1 thing to add to your bucket list.',
        type: 'dropdown',
        options: ['Bungee Jumping', 'Sky Diving']
      }
    ];

    let action = { type: 'ANSWER_QUESTION', id: 0, answer: '16' };
    Object.freeze(action);

    expect(questions(stateBefore, action)).toEqual(stateAfter);
  });
});

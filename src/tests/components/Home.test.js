import React from 'react';
import { shallow } from 'enzyme';
import { Home } from '../../components/Home/Home';

import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({ adapter: new Adapter() });

describe('Home Component', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(
      <Home currentQuestion={0} firstQuestionId={123} history={{}} />
    );
  });

  it('renders without crashing', () => {
    expect(wrapper.length).toEqual(1);
  });
});

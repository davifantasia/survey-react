export const TITLE = 'Survey Made With React';
export const QUESTION = 'Question:';
export const SUMMARY = 'Summary';
export const BACK = 'Back';
export const NEXT = 'Next';
export const FINISH = 'Finish';

const DATA = [
  {
    id: 12345,
    question: 'How old are you?',
    type: 'text'
  },

  {
    id: 123456,
    question: 'Which would you rather have.',
    type: 'dropdown',
    options: ['Dog', 'Cat', 'Bird', 'Snake']
  },

  {
    id: 1234567,
    question: 'Choose one.',
    type: 'radio',
    options: ['Tea', 'Coffee']
  }
];

export default DATA;

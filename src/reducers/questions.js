import DATA from '../data';

export default (state: Array<Object> = DATA, action: Object): Array<Object> => {
  switch (action.type) {
  case 'ANSWER_QUESTION':
    return state.map(question => {
      if (question.id === action.id) {
        return { ...question, answer: action.answer };
      }
      return question;
    });
  default:
    return state;
  }
};

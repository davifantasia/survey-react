import { combineReducers } from 'redux';
import questions from './questions';
import currentQuestion from './currentQuestion';

export default combineReducers({
  questions,
  currentQuestion
});

export default (state: number = 0, action: Object): number => {
  switch (action.type) {
  case 'SET_CURRENT_QUESTION':
    return action.id;
  default:
    return state;
  }
};
